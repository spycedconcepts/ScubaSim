const gulp = require("gulp");
const uglify = require("gulp-uglify");
const autoprefixer = require("gulp-autoprefixer");
const plumber = require("gulp-plumber");
const sourcemaps = require("gulp-sourcemaps");
const minifyCss = require("gulp-minify-css");
const concat = require("gulp-concat");
const sass = require("gulp-sass");

// local vars
const CSS_DIST_PATH = "./public/dist/css";
const SCRIPTS_DIST_PATH = "./public/dist/js";
const SCRIPTS_SOURCE = "./src/js/**/*.js";
const SCSS_SOURCE = "./src/scss/";

//Styles for SASS

gulp.task("scss", function() {
  console.log("Starting scss task");
  return gulp
    .src([SCSS_SOURCE + "styles.scss"])
    .pipe(
      plumber(function(e) {
        console.log("scss Task Error");
        console.log(e);
        this.emit("end");
      })
    )
    .pipe(sourcemaps.init())
    .pipe(autoprefixer())
    .pipe(
      sass({
        outputStyle: "compressed"
      })
    )
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(CSS_DIST_PATH));
});

//Scripts
gulp.task("scripts", function() {
  console.log("Starting scripts task");

  return gulp
    .src(SCRIPTS_SOURCE)
    .pipe(
      plumber(function(e) {
        console.log("Scripts Task Error");
        console.log(e);
        this.emit("end");
      })
    )
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(gulp.dest(SCRIPTS_DIST_PATH))
    .pipe(sourcemaps.write());
});

//Images
gulp.task("images", function() {
  console.log("Starting images task");
});

//Default
gulp.task("default", function() {
  console.log("Starting default task");
});

gulp.task("watch", function() {
  console.log("Starting gulp watch");
  require("./server.js");
  liveReload.listen();
  gulp.watch(SCRIPTS_SOURCE, ["scripts"]);
  //gulp.watch(CSS_PATH, ["styles"]);
  gulp.watch(SCSS_SOURCE + "**/*.scss", ["scss"]);
});

//README

/** A number of gulp modules don't suppor ES6 arrow functions, significantly uglify.
 * Use only exlpicity "function () " declarations in this gulpfile.
 */

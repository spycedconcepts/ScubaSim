# ScubaSim

This project was built as a demonstration of using ES5 code to build an object orientated Scuba Simulation engine.
At the time of this initial Git repo, the engine contains calcuations for descent/ascent rates and the impact on air consumption
caused by breathing gas at depth.  

I am not seeking to create a simulation game at this point, but will be more than happy to collaborate with any game asset designers
seeking to work with three.js rendering library.

## Roadmap

- Refactoring code base as ES6 code using babel to transpile to ES5.
- Implement unit testing using Mocha framework 
- Integrate with React.js and Node.Js to facilitate better rendering.
- Improve front end UI.
- Refactor as API service.
- Seek collaboration to build game front end based on sim model.

## In Action

A demo of the model is available at <https://scubasim.stulast.co.uk/>.  The server is setup to sleep after a period of inactivity, so may take a few seconds to spin up if it's been inactive for a while.  

## View and Fork the code

- <https://github.com/StuLast/ScubaSim>
